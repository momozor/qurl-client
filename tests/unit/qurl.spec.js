import { shallowMount } from '@vue/test-utils';
import Encoder from '@/components/Encoder.vue';

describe('Encoder.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(Encoder, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
